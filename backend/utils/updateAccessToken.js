//функция-запрос на обновление токена и файла токена
import fetch from "node-fetch";
import config from "config";
import fs from "fs";

const refreshTokenURL = config.get("tokenUrl");
const client_id = config.get("client_id");
const client_secret = config.get("client_secret");
const redirect_uri = config.get("redirect_uri");

export let updateAccessToken = async (refresh_token) => {
  let body = {
    client_id: `${client_id}`,
    client_secret: `${client_secret}`,
    grant_type: "refresh_token",
    refresh_token: `${refresh_token}`,
    redirect_uri: `${redirect_uri}`,
  };
  const response = await fetch(`${refreshTokenURL}`, {
    //кидаем запрос на новый access_token
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify(body),
  });
  const data = await response.json(); //получаем ответ
  await fs.promises.writeFile("./token.json", JSON.stringify(data)); //пишем ответ в файл
  let new_data = await fs.promises.readFile("./token.json");
  return JSON.parse(new_data.toString());
};
