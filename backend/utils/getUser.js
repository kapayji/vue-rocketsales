import fetch from "node-fetch";

import config from "config";

const baseURL = config.get("baseUrl");

export let getUser = async (access_token, leadUser) => {
  let user = await fetch(`${baseURL}/users/${leadUser}`, {
    method: "GET",
    headers: {
      "Content-type": "application/json",
      Authorization: `Bearer ${access_token}`,
    },
  });
  let user_info = await user.json();
  return { name: user_info.name };
};
