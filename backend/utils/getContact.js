import fetch from "node-fetch";

import config from "config";

const baseURL = config.get("baseUrl");

export let getContact = async (access_token, leadContactId) => {
  let contact = await fetch(`${baseURL}/contacts/${leadContactId}`, {
    method: "GET",
    headers: {
      "Content-type": "application/json",
      Authorization: `Bearer ${access_token}`,
    },
  });
  let contact_info = await contact.json();

  if (contact_info.custom_fields_values != null) {
    let custom_fields = [];

    let result = {};
    for (const field of contact_info.custom_fields_values) {
      let values_fields = [];
      for (const values of field.values) {
        values_fields.push({ value: values.value });
      }
      custom_fields.push({
        field_code: field.field_code,
        field_value: values_fields,
      });
    }

    result = { contact_info: contact_info.name, custom_fields };
    // console.log(result);
    return result;
  } else {
    return { contact_info: contact_info.name };
  }
};
