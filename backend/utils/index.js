import config from "config";
import jwt from "jsonwebtoken";
import fs from "fs";
import { updateAccessToken } from "./updateAccessToken.js";
import { fetchingUrl } from "./fetchingUrl.js";

const baseURL = config.get("baseUrl");
const client_secret = config.get("client_secret");

export let getDataFromURL = async (link) => {
  let token_data = await fs.promises.readFile("./token.json"); //читаем файл
  let data = JSON.parse(token_data.toString()); //приводим к формату JSON
  let access_token = data.access_token; //достаём access_token
  let refresh_token = data.refresh_token; //достаём refresh_token
  let decode = jwt.decode(access_token, client_secret);
  let jwt_expired_check = Date.now() >= decode.exp * 1000; //проверяем токен на истечение
  if (!jwt_expired_check) {
    //делаем запрос к link
    let response = await fetchingUrl(`${baseURL}/${link}`, access_token);
    return await response;
  } else {
    //в случае истечения токена перезапрашиваем access_token
    let response_refresh_token = await updateAccessToken(refresh_token);
    //делаем запрос к link
    let response = await fetchingUrl(
      `${baseURL}/${link}`,
      response_refresh_token.access_token
    );
    return response;
  }
};
