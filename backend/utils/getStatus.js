import fetch from "node-fetch";

import config from "config";

const baseURL = config.get("baseUrl");

export let getStatus = async (access_token, pipeline_id, leadStatusId) => {
  const status = await fetch(
    `${baseURL}/leads/pipelines/${pipeline_id}/statuses/${leadStatusId}`,
    {
      method: "GET",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
    }
  );
  const status_info = await status.json();
  return { name: status_info.name, color: status_info.color };
};
