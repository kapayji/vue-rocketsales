import fetch from "node-fetch";
import { getContact } from "./getContact.js";
import { getStatus } from "./getStatus.js";
import { getUser } from "./getUser.js";

//универсальная функция-запрос
export let fetchingUrl = async (link, access_token) => {
  try {
    const response = await fetch(`${link}`, {
      method: "GET",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${access_token}`,
      },
    });
    const lead_detail_filtered = [];
    const getting_data = await response.json();
    const response_leads = await getting_data._embedded.leads;
    //проходимся по сделкам
    for (const lead of response_leads) {
      //достаём информацию
      //об ответственном лице
      let user = await getUser(access_token, lead.responsible_user_id);
      //о  статусах
      let status = await getStatus(
        access_token,
        lead.pipeline_id,
        lead.status_id
      );

      let contacts = [];
      //проверяем есть ли контакты в сделке, если нет - вернёт пустой массив
      if (lead._embedded.contacts.length > 0) {
        //собираем информацию о контактах циклом
        for (const item of lead._embedded.contacts) {
          let contact = await getContact(access_token, item.id);
          contacts.push(contact);
        }
      }
      //собираем сделки с фильтрованной информацие в кучку для отправки
      lead_detail_filtered.push({
        name: lead.name,
        price: lead.price,
        date: new Date(lead.created_at * 1000).toLocaleDateString(),
        user,
        status,
        contacts,
      });
    }
    return lead_detail_filtered;
  } catch (e) {
    return [];
  }
};
