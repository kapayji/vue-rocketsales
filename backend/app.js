import express from "express";
import cors from "cors";
// import path from "path";
// const __dirname = path.resolve();

import { queryRoutes } from "./routes/query/query.routes.js";

const app = express();

app.use(express.static("assets"));

app.use(cors());

app.use(
  express.json({
    extended: true,
  })
);
app.use("/api/leads", queryRoutes);
async function start() {
  app.listen(5000, () => {
    console.log(`Start on 5000 port`);
  });
}

start();
