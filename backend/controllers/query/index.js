import { getDataFromURL } from "../../utils/index.js";

export async function getLeads(req, res) {
  try {
    const query = req.params.query || "";
    const response = await getDataFromURL(
      `leads?query=${encodeURI(query)}&with=contacts`
    );
    res.json(response);
  } catch (e) {
    res.status(400).json({ message: "Error" });
    console.log(e);
  }
}
