import Router from "express";
import { getLeads } from "../../controllers/query/index.js";

export const queryRoutes = Router();

queryRoutes.get("/:query?", async (req, res) => {
  getLeads(req, res);
});
